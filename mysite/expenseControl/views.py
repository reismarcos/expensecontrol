from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView, ListView, DetailView 
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Category, Transaction
from django.urls import reverse_lazy
from django.http import JsonResponse
import json
from django.shortcuts import render_to_response

def home(request):
    balance = 0
    if request.user.is_authenticated:
        transactions = Transaction.objects.filter(user=request.user)
        for t in transactions:
            balance += t.value
    return render(request, 'home.html', {
        'balance': balance
    })

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {
        'form': form
    })

class CategoryList(ListView):
    template_name = './category/category_list.html'
    model = Category

    def get_queryset(self):
        return Category.objects.filter(user=self.request.user)

class CategoryDetail(DetailView):
    model = Category
    template_name = './category/category_detail.html'

class CategoryCreate(CreateView):
    model = Category
    fields = ['name']
    template_name = './category/category_form.html'
    success_url = reverse_lazy('category_list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CategoryCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Criar'
        return context
    

class CategoryUpdate(UpdateView): 
    model = Category
    fields = ['name']
    template_name = './category/category_form.html'
    success_url = reverse_lazy('category_list')
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Editar'
        return context
    

class CategoryDelete(DeleteView): 
    model = Category
    template_name = './category/category_confirm_delete.html'
    success_url = reverse_lazy('category_list')

class TransactionList(ListView):
    template_name = './transaction/transaction_list.html'
    model = Transaction
    def get_context_data(self, **kwargs):
        transactions = Transaction.objects.filter(user=self.request.user)
        balance = 0
        for t in transactions:
            balance += t.value
        context = super().get_context_data(**kwargs)
        context['title'] = 'Transações'
        context['balance'] = balance
        return context
    
    def get_queryset(self):
        return Transaction.objects.filter(user=self.request.user).order_by('-date')

class ExpenseList(ListView):
    template_name = './transaction/transaction_list.html'
    model = Transaction
    def get_context_data(self, **kwargs):
        transactions = Transaction.objects.filter(user=self.request.user)
        balance = 0
        for t in transactions:
            balance += t.value
        context = super().get_context_data(**kwargs)
        context['title'] = 'Despesas'
        context['balance'] = balance

        return context
    
    def get_queryset(self):
        return Transaction.objects.filter(user=self.request.user, value__lt = 0).order_by('-date')

class IncomeList(ListView):
    template_name = './transaction/transaction_list.html'
    model = Transaction

    def get_context_data(self, **kwargs):
        transactions = Transaction.objects.filter(user=self.request.user)
        balance = 0
        for t in transactions:
            balance += t.value
        context = super().get_context_data(**kwargs)
        context['title'] = 'Rendas'
        context['balance'] = balance

        return context
    
    def get_queryset(self):
        return Transaction.objects.filter(user=self.request.user, value__gt = 0).order_by('-date')        

class TransactionDetail(DetailView):
    model = Transaction
    template_name = './transaction/transaction_detail.html'

class ExpenseCreate(CreateView):
    model = Transaction
    fields = ['value','category','description','date']
    template_name = './transaction/transaction_form.html'
    success_url = reverse_lazy('expense_list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.value = form.instance.value * -1
        return super(ExpenseCreate, self).form_valid(form)
        
    def get_form(self, *args, **kwargs):
        form = super(ExpenseCreate, self).get_form(*args, **kwargs)
        form.fields['category'].queryset = Category.objects.filter(user=self.request.user)
        return form
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Criar despesa'
        return context
    

class IncomeCreate(CreateView):
    model = Transaction
    fields = ['value','category','description','date']
    template_name = './transaction/transaction_form.html'
    success_url = reverse_lazy('income_list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(IncomeCreate, self).form_valid(form)

    def get_form(self, *args, **kwargs):
        form = super(IncomeCreate, self).get_form(*args, **kwargs)
        form.fields['category'].queryset = Category.objects.filter(user=self.request.user)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Criar renda'
        return context
    
    

class TransactionUpdate(UpdateView): 
    model = Transaction
    fields = ['value','category','description']
    template_name = './transaction/transaction_form.html'
    success_url = reverse_lazy('transaction_list')
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Editar'
        return context
    

class TransactionDelete(DeleteView): 
    model = Transaction
    template_name = './transaction/transaction_confirm_delete.html'
    success_url = reverse_lazy('transaction_list')

class UserUpdate(UpdateView): 
    model = User
    fields = ['first_name', 'last_name', 'email']
    template_name = './user/user_form.html'
    success_url = reverse_lazy('transaction-list')

def report(request):
    queryset = Category.objects.filter(user=request.user)
    for c in queryset:
        transactions_list = Transaction.objects.filter(category = c)
        for t in transactions_list:
            c.spend += t.value

    names = [obj.name for obj in queryset]
    values = [int(obj.spend) for obj in queryset]

    transaction_list = Transaction.objects.filter(user=request.user).order_by('-value')
    maxSpend = 0
    maxIncome = 0
    if len(transaction_list) > 0:
        maxSpend = transaction_list[(len(transaction_list))-1],
        maxIncome = transaction_list[0]

    context = {
        'names': json.dumps(names),
        'values': json.dumps(values),
        'maxSpend': maxSpend,
        'maxIncome': maxIncome,
    }
    return render(request, 'report.html', context)