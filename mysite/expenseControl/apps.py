from django.apps import AppConfig


class ExpensecontrolConfig(AppConfig):
    name = 'expenseControl'
