from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    spend = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    def __str__(self):
        return self.name
    
    def overallSpend(self):
        transactions = Transaction.objects.filter(category = self.id)
        overall = 0
        for i in transactions:
            overall += i.value
        return overall

    def getTransactions(self):
        return Transaction.objects.filter(category = self.id).order_by('-date')

    

class Transaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=now)
    description = models.TextField(max_length=400, default='')
    value = models.DecimalField(max_digits=8, decimal_places=2)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    def __str__(self):
        return 'valor: ' + str(self.value) + ' | ' + 'data:' + str(self.date)



    