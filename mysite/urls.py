from django.contrib import admin
from django.urls import path, include

from mysite.expenseControl import views 

urlpatterns = [
    path('', views.home, name='home'),
    path('signup/', views.signup, name='signup'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
    path('categorias', views.CategoryList.as_view(), name='category_list'),
    path('categorias/<int:pk>', views.CategoryDetail.as_view(), name='category_detail'),
    path('categorias/criar', views.CategoryCreate.as_view(), name='category_create'),
    path('categorias/editar/<int:pk>', views.CategoryUpdate.as_view(), name='category_update'),
    path('categorias/deletar/<int:pk>', views.CategoryDelete.as_view(), name='category_delete'),
    path('transacoes', views.TransactionList.as_view(), name='transaction_list'),
    path('transacoes/despesas', views.ExpenseList.as_view(), name='expense_list'),
    path('transacoes/rendas', views.IncomeList.as_view(), name='income_list'),
    path('transacoes/<int:pk>', views.TransactionDetail.as_view(), name='transaction_detail'),
    path('transacoes/despesas/criar', views.ExpenseCreate.as_view(), name='expense_create'),
    path('transacoes/rendas/criar', views.IncomeCreate.as_view(), name='income_create'),
    path('transacoes/editar/<int:pk>', views.TransactionUpdate.as_view(), name='transaction_update'),
    path('transacoes/deletar/<int:pk>', views.TransactionDelete.as_view(), name='transaction_delete'),
    path('user/editar/<int:pk>', views.UserUpdate.as_view(), name='user_update'),
    path('report', views.report, name='report'),

]